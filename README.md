# Documentation

Die vorliegende Dokumentation des Projekt Q umfasst drei Projektphasen:  
[1. Vision & Konzept](https://gitlab.com/project-q/documentation/wikis/1/%C3%9Cberblick)  
[2. Entwicklung](https://gitlab.com/project-q/documentation/wikis/2/%C3%9Cberblick)  
[3. Forschung, Evaluation, Assessment, Verwertung](https://gitlab.com/project-q/documentation/wikis/3/%C3%9Cberblick)